# Workflow of deploying Snowflake External function in AWS

### Date: Apr-2021

## The back story
Snowflake External Functions facilitates in developing solutions like invoking external APIs, third-party libraries integration, etc... An example solution could be the need to extract data from a PDF or fetch a currency conversion from OANDA or parse and extract metadata from a video in S3 buckets etc...

Recently I had been presented with a scenario, which required one of the above-mentioned scenarios. I have been off-putting the need to develop external functions for some time and finally took the plunge. Going over the [Doc: Snowflake External Function](https://docs.snowflake.com/en/sql-reference/external-functions.html), It was very clear on what's involved and the ins and outs of adopting the external function.

However when it came to actually do a simple prototype implementation in AWS, based on [Doc: Creating External Functions on AWS](https://docs.snowflake.com/en/sql-reference/external-functions-creating-aws-options.html), it became a little daunting. Don't get me wrong, the document itself was clear on the steps, but it was lacking visuals.

If you are trying to develop your prototype based on the doc, understand that it can be done as long as you are in control of the AWS and Snowflake environment. This is however not the case when it comes to organizations (ex: enterprises) where there are clear distinctions on who can do what and how.

I did end up implementing the prototype, to aid my fellow implementors, I thought I will share some perspectives on various personas/roles involved in developing this. The [Doc: Creating External Functions on AWS](https://docs.snowflake.com/en/sql-reference/external-functions-creating-aws-options.html), from Snowflake is the golden authority still.

## Personas

Personas are people with different roles and have different capabilities. Here is the list of personas involved in implementing the Snowflake External Function. The list will refer to a set of steps that involved based of Creating an External Function on AWS Using the Web Interface

**AWS Developer:** Implements the functionality in the cloud, either it is AWS Lambda or a docker running in Fargate, etc is based on the need. The person also defines the API Gateway, which will be used as the proxy service between Snowflake and the resulting function. The person is also responsible for ensuring the deployment of these components in AWS. Steps Involved : 1 & 2 (partially)

**AWS Admin (IAM):** Depending on the org this could be a system admin or IAM admin who creates and manages roles & permissions etc. Steps Involved: 2 (partially)

**Snowflake Admin:** API Integrations can be created by Account Admin or a role with CREATE INTEGRATION privilege. In larger org, the ACCOUNTADMIN is not necessarily the DBA. It could be a separate department or a business leader etc... Hence anticipate some extra coordination efforts. The person creates an API Integration based on inputs from the AWS developer. Steps Involved: 2 (partially)
Snowflake Developer: Defines the external function and uses it. Steps Involved: 3 & 4

## Workflow

Based on, [Creating an External Function on AWS Using the Web Interface](https://docs.snowflake.com/en/sql-reference/external-functions-creating-aws.html), the below workflow diagram puts the necessary steps involved and brings them into some perspective.

![](./media/workflow_sflk_external.png)

```plantuml 
@startuml
skinparam backgroundColor white

skinparam sequence {
ArrowColor DeepSkyBlue
ActorBorderColor DeepSkyBlue
LifeLineBorderColor blue
LifeLineBackgroundColor #A9DCDF

ParticipantBorderColor DeepSkyBlue
ParticipantBackgroundColor DodgerBlue
ParticipantFontName Impact
ParticipantFontSize 17
ParticipantFontColor #A9DCDF

ActorBackgroundColor aqua
ActorFontColor DeepSkyBlue
ActorFontSize 17
ActorFontName Aapex
}

actor "AWS_Admin" as aws_admin #red
participant "AWS IAM" as aws_iam

actor "AWS Developer" as aws_dev #brown
participant "AWS CloudFormation" as aws_cf

actor "Snowflake Admin" as sflk_admin #blue
participant "Snowflake Account" as slfk_account

actor "Snowflake Developer" as sflk_dev #green
database "Snowflake DB" as slfk_db

== Phase : create roles ==
aws_admin -> aws_iam: create roles
aws_admin -> aws_dev : inform roles to be used

== Phase : create api & functions ==
aws_dev -> aws_cf: create functions
aws_dev -> aws_cf: create api gateway

== Phase : register api integration ==
aws_dev -> sflk_admin : inform roles, api url
sflk_admin -> slfk_account : register api integration

== Phase : Update IAM role trust policy ==
sflk_admin -> aws_admin : inform Snowflake integration value (API_AWS_IAM_USER_ARN, API_AWS_EXTERNAL_ID)
aws_admin -> aws_iam : update IAM role, used by Snowflake, with trust policy

== Phase : Create external functions ==
sflk_admin -> slfk_db : grant usage of Snowflake api integration to Snowflake role
aws_dev -> sflk_dev : inform api url
sflk_dev -> slfk_db : create external function

== Phase : Use external functions ==
sflk_dev -> slfk_db : use external function

@enduml
```


## Missing Personas

Depending on the org, additional personas may come into play like the below.

**AWS VPC Administrator:** If Snowflake is integrated via a private link, then routes have to be defined between API gateway and Snowflake. This person would need to be involved.

**Snowflake DBA:** If not the developer, the DBA might need to be involved in defining the external function in the database/schema of choice.
DevOps: Involving DevOps as soon as a simple integration is prototyped would be recommended, as they would need to put in efforts on automation.

## Gotchas

- While during development, if the API endpoint changes, then are prepared to do the whole exercise again. This is mainly because the Snowflake API integration would need to be updated, which changes the 'API_AWS_EXTERNAL_ID'. This intern would require 'Trust relation' to be updated for the role Snowflake assumes.
- Unless you have done this once or twice, be prepared to have a war-room strategy session with all the parties in the room and develop the prototype.
- It would be beneficial to assign the 'CREATE INTEGRATION' into a custom role, as future updates could require less interaction with the Account Admin. It would also aid with the DevOps pipeline implementation.
- Determining ahead of time if the function needs to be Synchronous or Asynchronous would avoid some headaches. In the case of Asynchronous implementation, more AWS services could be involved.
- I would not promise that things will work on the first try, there are definitely some efforts involved.

## How can the effort be improved?

There are some ways to automate this end-to-end but would require some consideration and openness to be adopted across all the involved parties, to put the steps into a DevOps pipeline.

## Here are some guides:

- [Video of deployment steps from Snowflake](https://youtu.be/qangh4oM_zs)
- [Basic cloud formation template from Snowflake](https://github.com/Snowflake-Labs/sfguide-external-functions-examples/blob/main/DeploymentTemplates/aws/BasicSetup.yaml)
- [Screen by screen walk thru](https://interworks.com/blog/2020/08/14/zero-to-snowflake-setting-up-snowflake-external-functions-with-aws-lambda/)
s