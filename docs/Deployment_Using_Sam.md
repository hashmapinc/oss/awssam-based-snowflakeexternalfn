
# Deploying Synchronous External Function using AWS SAM

Previously, I had blogged on [Workflow of deploying Snowflake External function in AWS](https://www.linkedin.com/pulse/workflow-deploying-snowflake-external-function-aws-venkatesh-sekar/?trackingId=m3cb%2F9FgQzq5oOSJcxHF2A%3D%3D). Developing
a cloud formation script, could aid in the deployment process. Snowflake has provided cloud formation templates in its [Repo : sfguide-external-functions-examples](https://github.com/Snowflake-Labs/sfguide-external-functions-examples/blob/main/DeploymentTemplates/aws/BasicSetup.yaml). This template offers a good starting point, If you want to adopt this, would recommend to understand and make appropriate changes.

## AWS SAM approach
AWS recommended approach for developing serverless functionality is to use [AWS SAM/Serverless Application Model](https://aws.amazon.com/serverless/sam/). The above basic template however would not work well, if your team uses AWS SAM.

Hence to help my community, I am sharing an example SAM template code base which will :
 - create roles used by lambda with the 'AWSLambdaBasicExecutionRole' privilege.
 - create lambda function & api gateway
 - configure the api gateway with AWS_IAM as the authorizer
 - define resource policies
 - create role, which will be assumed by Snowflake, with base trust defined.
The aim is in no way to override Snowflake provided templates. This is shared in the hope that it will reduce 
some work effort in figuring out how to develop the cloud formation for SAM.

**NOTE:** 
 - If you decide to use this template, please take the time to make appropriate changes; like names etc.. 
as per your needs.
 - It is assumed, that you know and are experienced with cloudformation templates and AWS SAM etc...
 - The walk thru demonstrated here is not tested against Snowflake connected via private link or private VPC. Hence do expect some level of effort when using this example template.

## Deployment Steps

### Step 1: Create base artifacts
This step is to be performed by an AWS IAM admin or by a person who has IAM Role create permissions. The cloud
formation script [0-sflk-extnfn-base.yaml](../0-sflk-extnfn-base.yaml), would create
  - An S3 bucket, which will be used to host the Lambda code etc...
  - A role, which will be used by Lambda. This definition is part of Logical ID : 'FnExecRole'.
  - A role, which will be assumed by Snowflake to invoke the methods at API end point. This definition is part of Logical ID : 'SflkExecFnAPICallerRole'.

![](./media/sflk-extnfn-base_cfstack.png)

To make it simpler for the admins, the SflkExecFnAPICallerRole is preconfigured with a template of the trust policy, with appropriate placeholders.

![](./media/sflk_role_predefined_template.png)


### Step 2: Develop Functionality
This step is performed by AWS Developer who uses AWS SAM to develop
the lambda function and the API Gateway integration. The lambda implementation
code that is presented is based of the example from the Snowflake documentation. 

#### Update the parameters 
To deploy this SAM, you need to retrieve the value of the following keys from
the previously deployed CF stack.

  - FnExecRole
  - SflkExecFnAPICallerRole

It is not necessary to use the CF stack, If your AWS Admin created these roles manually, ask for the ARN's.

![](./media/sflk-extnfn-base_output.png)
  
Subtitute the value in the 'parameter_overrides' section of [samconfig.toml](../samconfig.toml)

#### Deploy
Once ready, you can do the deployment as follows :

```sh
sam build -t 1-synchronous_fn_template.yaml

sam deploy -t 1-synchronous_fn_template.yaml
```

Here is the list of resources deployed from the stack: 
![](./media/sflk-extnfn-echo.png)

#### Status of what got deployed
<details>
  <summary>Click to expand!</summary>

As mentioned in the Snowflake steps, the rest api method is defined with AWS_IAM.
![](./media/Authorizer_defined.png)

The resource policy is also defined:
![](./media/resource_policy_defined.png)

The stage 'v0' is defined as follows:
![](./media/defined_stage.png)
</details>

### Step 3: Define API Integeration
This step is to be done by the Snowflake Admin who has 'CREATE INTEGRATION' privillege. For this the url from the API gateway is to be supplied.

![](./media/create_api_integration.png)

Also ensure to grant usage of the integration to the roles used by the developers

```sql
USE role securityadmin;
GRANT usage on integration aws_sflk_api_integration_venkat to role sysadmin;
GRANT usage on integration aws_sflk_api_integration_venkat to role DEVELOPER;
```

### Step 4: Update Trust relationship 
This step is to be done by the AWS admin, updating the trust relationship for the
role that will be assumed by Snowflake 'sflk-extnfn-base-dev-sflkapi-caller-role'.

![](./media/trust_relationship.png)

### Step 5: Create external function 
This step is done by the Snowflake developer. Using the API link, the person would
create the function. Once defined, you can invoke with a simple select to this the flow.

![](./media/call_external_function.png)


