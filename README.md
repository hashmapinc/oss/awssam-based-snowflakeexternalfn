# AWSSAM-based-SnowflakeExternalFn

A basic set of AWS SAM templates for implementing Snowflake external functions.

## Documentation
 - [Workflow of deploying Snowflake External function in AWS](./docs/Deployment_Workflow.md)

- [Deploying Synchronous External Function using AWS SAM](./docs/Deployment_Using_Sam.md)

